import 'package:flutter/material.dart';
import 'package:jago_webview/LinkJAGO.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  dynamic bankAccount;

  void _incrementCounter() {
    showModalBottomSheet(
      context: context,
      builder: (context) => Container(
        child: RaisedButton(
          child: Text('Link account'),
          onPressed: () async {
            final navigator = Navigator.of(context);
            final bankAccount = await navigator
                .push(MaterialPageRoute(builder: (_) => LinkJAGO()));
            setState(() {
              this.bankAccount = bankAccount;
            });
            navigator.pop();
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: bankAccount == null
            ? Container()
            : Column(
                children: <Widget>[
                  ListTile(
                    title: Text('accountName: ${bankAccount['accountName']}'),
                    subtitle:
                        Text('accountNumber: ${bankAccount['accountNumber']}'),
                  )
                ],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
