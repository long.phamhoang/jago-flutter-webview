import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class LinkJAGO extends StatefulWidget {
  @override
  _LinkJAGOState createState() => _LinkJAGOState();
}

class _LinkJAGOState extends State<LinkJAGO> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
      name: 'JAGO',
      onMessageReceived: (JavascriptMessage message) {
        Navigator.pop(context, jsonDecode(message.message));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('JAGO accounts'),
      ),
      body: WebView(
        initialUrl: 'http://172.16.51.53:3000/link-merchant',
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
        },
        javascriptChannels: <JavascriptChannel>[
          _toasterJavascriptChannel(context),
        ].toSet(),
        gestureNavigationEnabled: true,
      ),
    );
  }
}
